# Haskell implementation of AES

# To build the project
stack build

# To run the code
stack exec aes-exe

# To test the code
stack test

# To generate the documentation
stack exec -- haddock --html src/\*.hs src/\*/\*.hs src/\*/\*/\*.hs app/Main.hs --hyperlinked-source --odir=dist/docs
