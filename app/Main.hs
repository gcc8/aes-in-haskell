module Main (main) where

import EncryptBlock
import DecryptBlock

masterKey = [[0x00, 0x11, 0x22, 0x33], [0x44, 0x55, 0x66, 0x77], [0x88, 0x99, 0xAA, 0xBB], [0xCC, 0xDD, 0xEE, 0xFF]]
block = [[248,249,250,251], [252,253,254,255], [56,57,65,66], [67,68,69,70]] -- Some dummy block
nbRounds = 10

-- | A simple example to test this AES haskell implementation
main :: IO ()
main = do
        print $ "Intial block: " ++ show block
        let encryptedBlock = encrypt nbRounds masterKey block
        print $ "Encrypted block: " ++ show encryptedBlock
        let decryptedBlock = decrypt nbRounds masterKey encryptedBlock
        print $ "Decrypted block: " ++ show decryptedBlock
