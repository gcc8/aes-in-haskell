module KeySchedule
    ( -- * Functions 
      getRoundKeys
    ) where

import Control.Monad (replicateM)
import Control.Monad.State.Lazy (State, put, get, evalState)
import Data.Bits (xor)
import Data.List (transpose)
import Numeric (showHex)

import Common (Key, BlockMatrix, KeyMatrix, NbRounds, RoundNo, nbRounds, aesPoly)
import Utils.List.Operations (xors, rotate)
import SBox (applySbox)

-- | Round constants
rc :: RoundNo -> Integer
rc i
    | i == 1 = 1
    | i > 1 && rcPrev < 0x80 = 2 * rcPrev
    | otherwise = (2 * rcPrev) `xor` aesPoly
    where rcPrev = rc (i-1)

-- | rotWord [a0, a1, a2, a3] = [a1, a2, a3, a0]
rotWord = rotate 1

-- | subWord [a0, a1, a2, a3] = [S(a0), S(a1), S(a2), S(a3)]
subWord = map applySbox

-- | Algorithm to get the subkey using previous round
-- Here w1, w2, w3 and w4 are the columns of the key matrix
-- i is the round number (starts at 1)
computeRoundKey :: RoundNo -> KeyMatrix -> KeyMatrix
computeRoundKey i [w0, w1, w2, w3] = [w0', w1', w2', w3']
    where w3s = subWord . rotWord $ w3
          rconi = [rc i, 0x0, 0x0, 0x0]
          w3t = w3s `xors` rconi
          w0' = w3t `xors` w0
          w1' = w0' `xors` w1
          w2' = w1' `xors` w2
          w3' = w2' `xors` w3

-- | Round key generator
roundKeyGenerator :: State (RoundNo, KeyMatrix) KeyMatrix
roundKeyGenerator = do
    (i, roundKey) <- get
    let keyT = transpose roundKey -- organize key by column
    let newKeyT = computeRoundKey i keyT -- algo to find the round key
    let newRoundKey = transpose newKeyT -- organize key by row
    put (i+1, newRoundKey)
    return newRoundKey

-- | Get all round keys from the master key
getRoundKeys :: NbRounds -> KeyMatrix -> [KeyMatrix]
getRoundKeys n key = evalState (replicateM n roundKeyGenerator) s0
    where s0 = (1, key)

-- | Print the key matrix as a flat list in hexadecimal
ppHex keyM = map (("0x" ++) . (flipShowHex "")) $ concat keyM
    where flipShowHex = flip showHex

    