module Common
    ( -- * Types
      Key
    , BlockMatrix
    , KeyMatrix
    , NbRounds
    , RoundNo
      -- * Constants
    , nbRounds
    , aesPoly
      -- * Functions
    , decToHex
    ) where

import Crypto.Number.F2m (BinaryPolynomial)
import Text.Printf (printf)

type NbRounds = Int
type RoundNo = Int
type Key = [Integer]
type BlockMatrix = [[Integer]]
type KeyMatrix = [[Integer]]

nbRounds :: Int
nbRounds = 10  -- ^ number of rounds for subkeys

aesPoly :: BinaryPolynomial
aesPoly = 0x11B  -- ^ 283 or 0b100011011 -- Rinjdael field: ZZ_2[x] / (x^8 + x^4 + x^3 + x + 1)

decToHex :: Integer -> String
decToHex = printf "0x%02X"
