module DecryptBlock
    ( -- * Functions
      decrypt
    ) where

import Control.Monad.State.Lazy (State, put, get)

import Common (BlockMatrix, KeyMatrix, NbRounds, RoundNo)
import KeySchedule (getRoundKeys)
import Operation.AddRoundKey (addRoundKey)
import Operation.MixColumns (mixColumnsInv)
import Operation.ShiftRows (shiftRowsInv)
import Operation.SubBytes (subBytesInv)
import Utils.Monad (repeatRounds)

-- | First step decryption where the input key is the master key
decryptStart :: KeyMatrix -> BlockMatrix -> BlockMatrix
decryptStart key = subBytesInv . shiftRowsInv . addRoundKey key

-- | One round decryption
decryptRound :: KeyMatrix -> BlockMatrix -> BlockMatrix
decryptRound roundKey = subBytesInv . shiftRowsInv . mixColumnsInv . addRoundKey roundKey

-- | Many rounds decryption state monad
decryptRoundGenerator :: [KeyMatrix] -> State (RoundNo, BlockMatrix) BlockMatrix
decryptRoundGenerator roundKeysInv = do
    (i, block) <- get
    let roundKey = roundKeysInv !! (i-1)
    let newBlock = decryptRound roundKey block
    put (i+1, newBlock)
    return $ newBlock

-- | Final step decryption
decryptEnd :: KeyMatrix -> BlockMatrix -> BlockMatrix
decryptEnd = addRoundKey

-- | Full schema for decryption with start + many rounds + end
decrypt :: NbRounds -> KeyMatrix -> BlockMatrix -> BlockMatrix
decrypt n masterKey block = decryptEnd masterKey lastBlock
    where roundKeysInv = reverse $ getRoundKeys n masterKey
          lastRoundKey = roundKeysInv !! 0
          firstBlock = decryptStart lastRoundKey block
          s0 = (1, firstBlock)
          lastBlock = repeatRounds n (decryptRoundGenerator roundKeysInv) s0
