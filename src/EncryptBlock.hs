module EncryptBlock
    ( -- * Functions
      encrypt
    ) where

import Control.Monad.State.Lazy (State, put, get)

import Common (BlockMatrix, KeyMatrix, NbRounds, RoundNo)
import KeySchedule (getRoundKeys)
import Operation.AddRoundKey (addRoundKey)
import Operation.MixColumns (mixColumns)
import Operation.ShiftRows (shiftRows)
import Operation.SubBytes (subBytes)
import Utils.Monad (repeatRounds)

-- | The first step involves the master key
encryptStart :: KeyMatrix -> BlockMatrix -> BlockMatrix
encryptStart = addRoundKey

-- | One round encryption
encryptRound :: KeyMatrix -> BlockMatrix -> BlockMatrix
encryptRound roundKey = addRoundKey roundKey . mixColumns . shiftRows . subBytes

-- | Many rounds encryption state monad
encryptRoundGenerator :: [KeyMatrix] -> State (RoundNo, BlockMatrix) BlockMatrix
encryptRoundGenerator roundKeys = do
    (i, block) <- get
    let roundKey = roundKeys !! (i-1)
    let newBlock = encryptRound roundKey block
    put (i+1, newBlock)
    return $ newBlock

-- | Final step encryption
encryptEnd :: KeyMatrix -> BlockMatrix -> BlockMatrix
encryptEnd roundKey = addRoundKey roundKey . shiftRows . subBytes

-- | Full schema for encryption with start + many rounds + end
encrypt :: NbRounds -> KeyMatrix -> BlockMatrix -> BlockMatrix
encrypt n masterKey block = encryptEnd lastRoundKey lastBlock
    where firstBlock = encryptStart masterKey block
          roundKeys = getRoundKeys n masterKey
          s0 = (1, firstBlock)
          lastBlock = repeatRounds n (encryptRoundGenerator roundKeys) s0
          lastRoundKey = roundKeys !! (n-1)

