module Operation.AddRoundKey
    ( -- * Functions
      addRoundKey
    ) where

import Data.List.Split (chunksOf)

import Common (BlockMatrix, KeyMatrix)
import Utils.List.Operations (xors)

-- | xor between a round key and a block
addRoundKey :: KeyMatrix -> BlockMatrix -> BlockMatrix
addRoundKey xxs yys = chunksOf (length xxs) tmp
    where tmp = (concat xxs) `xors` (concat yys)
