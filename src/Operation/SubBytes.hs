module Operation.SubBytes
    ( -- * Functions
      subBytes
    , subBytesInv
    ) where

import Common (BlockMatrix)
import SBox (applySbox, applySboxInv)

-- | Apply the Sbox to the block
subBytes :: BlockMatrix -> BlockMatrix
subBytes matrix = [map applySbox row | row <- matrix]

-- | Inverse operation
subBytesInv :: BlockMatrix -> BlockMatrix
subBytesInv matrix = [map applySboxInv row | row <- matrix]
