module Operation.MixColumns
    ( -- * Functions
      mixColumns
    , mixColumnsInv
    ) where

import Crypto.Number.F2m (addF2m, mulF2m, modF2m)
import Data.List (transpose)

import Common (BlockMatrix, aesPoly)

type Vector = [Integer]
type Matrix = [[Integer]]

-- | Matrices used for mixing columns
mc :: BlockMatrix
mc = [[0x02, 0x03, 0x01, 0x01], [0x01, 0x02, 0x03, 0x01], [0x01, 0x01, 0x02, 0x03], [0x03, 0x01, 0x01, 0x02]]
--
mcInv :: BlockMatrix
mcInv = [[0x0E, 0x0B, 0x0D, 0x09], [0x09, 0x0E, 0x0B, 0x0D], [0x0D, 0x09, 0x0E, 0x0B], [0x0B, 0x0D, 0x09, 0x0E]]

multiplyInField :: Vector -> Vector -> Vector
multiplyInField v w = zipWith (\x y -> mulF2m aesPoly x y) v w

dotProductInField :: Vector -> Vector -> Integer
dotProductInField v w = modF2m aesPoly result
    where result = foldl addF2m 0 $ multiplyInField v w

matrixProductInField :: Matrix -> Matrix -> Matrix
matrixProductInField m1 m2 = [map (dotProductInField row) (transpose m2) | row <- m1]

-- | Mix columns of a block
mixColumns :: BlockMatrix -> BlockMatrix
mixColumns = matrixProductInField mc

-- | Inverse operation
mixColumnsInv :: BlockMatrix -> BlockMatrix
mixColumnsInv = matrixProductInField mcInv
