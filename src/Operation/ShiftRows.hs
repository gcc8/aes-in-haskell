module Operation.ShiftRows
    ( -- * Functions
      shiftRows
    , shiftRowsInv
    ) where

import Data.Word (Word8)
import Utils.List.Operations (rotate)

import Common (BlockMatrix)

-- | Shift the rows of a block cyclically to the left by 0, 1, 2 and 3 places
shiftRows :: BlockMatrix -> BlockMatrix
shiftRows matrix = [rotate i rows | (i, rows) <- zip [0..] matrix]

-- | Inverse operation
shiftRowsInv :: BlockMatrix -> BlockMatrix
shiftRowsInv matrix = [rotate (n-i) rows | let n = length matrix, (i, rows) <- zip [0..n] matrix]
