module SBox
    ( applySbox
    , applySboxInv
    ) where

import Crypto.Number.F2m (BinaryPolynomial, addF2m, mulF2m, modF2m, invF2m, divF2m)
import Data.Maybe (fromJust)

import Common (aesPoly)

sboxPoly :: BinaryPolynomial
sboxPoly = 0x101 -- 257 or 0b100000001 -- Polynomial ring for the sbox: ZZ_2[x] / (x^8 + 1)

-- | This is the inverse of b in the Rinjdael field
inverse :: Integer -> Maybe Integer
inverse byte
    | byte == 0 = Just 0
    | otherwise = invF2m aesPoly byte

a :: Integer
a = 0x1F -- 31 or 0b11111 -- y^4 + y^3 + y^2 + y + 1 in the Sbox ring

c :: Integer
c = 0x63 -- 99 or 0b1100011 -- y^6 + y^5 + y + 1 in the Sbox ring

-- | Get the sbox value using an affine transform
applySbox :: Integer -> Integer
applySbox byte = modF2m sboxPoly q
    where b' = inverse byte
          ab' = mulF2m sboxPoly a $ fromJust b'
          q = addF2m ab' c

-- | Inverse operation
applySboxInv :: Integer -> Integer
applySboxInv q = fromJust $ inverse $ fromJust b'
    where num = addF2m q c
          b' = divF2m sboxPoly num a
