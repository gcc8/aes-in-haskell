module Utils.Monad
    ( -- * Functions
      repeatRounds
    ) where

import Control.Monad.State.Lazy (State, runState)

type NbRounds = Int
type RoundNo = Int
type Matrix = [[Integer]]

-- | Return the block obtained after several rounds
-- (Intermediate results are not kept).
repeatRounds :: NbRounds -> State (RoundNo, Matrix) Matrix -> (RoundNo, Matrix) -> Matrix
repeatRounds 0 _ s = snd s
repeatRounds n m s = repeatRounds (n-1) m s'
    where (r', s') = runState m s

