module Utils.List.Operations
    ( -- * Functions
      xors
    , rotate
    , dotProduct
    , matrixProduct
    ) where

import Data.Bits (xor)
import Data.List (transpose)

-- | xor between two row vectors
xors :: [Integer] -> [Integer] -> [Integer]
-- xors :: Integral a => [a] -> [a] -> [a] -- this one is not working
xors = zipWith xor

-- | Circularly rotate elemets in a list
rotate :: Int -> [a] -> [a]
rotate n xs = take (length xs) (drop n (cycle xs))

-- | Classical dot product between two vectors
dotProduct :: Integral a => [a] -> [a] -> a
dotProduct v w = sum $ zipWith (*) v w -- works as well
-- dotProduct = foldl' (+) 0

-- | Matrix product using list of lists
matrixProduct :: Integral a => [[a]] -> [[a]] -> [[a]]
matrixProduct m n = [map (dotProduct row) (transpose n) | row <- m]
