module Utils.Binary
    ( -- * Types
      Bit
      -- * Functions
    , xor
    , add
    , bin2int
    , bin2intR
    , int2bin
    , int2binR
    , int2binPad
    , make
    ) where

type Bit = Int
type Padding = Int

xor :: Bit -> Bit -> Bit
xor 0 0 = 0
xor 1 0 = 1
xor 0 1 = 1
xor 1 1 = 0

-- | Addition modulo 2 applied to two lists
add :: [Bit] -> [Bit] -> [Bit]
add = zipWith xor

-- |  "b2 b1 b0" is represented as [b0, b1 b2] (head of list = LSB)
-- >>> [1,1,0,1] = 11 -- 0b1011
bin2intR :: [Bit] -> Int
bin2intR = foldr (\x y -> x + 2*y) 0

-- |  "b2 b1 b0" is represented as [b2, b1 b0] (head of list = MSB)
-- >>> [1,0,1,1] = 11 -- 0b1011
bin2int :: [Bit] -> Int
bin2int = foldl (\x y -> 2*x + y) 0

-- >>> int2binR 11 = [1,1,0,1]
int2binR :: Int -> [Bit]
int2binR 0 = []
int2binR n = n `mod` 2 : int2binR (n `div` 2)

-- >>> int2bin 11 = [1,0,1,1]
int2bin = reverse . int2binR

-- | Number of binary digits in a decimal = floor(log2(n)) + 1
countBinDigits :: Int -> Int
countBinDigits n = (+1) . floor $ logBase 2 (fromIntegral n :: Float)

-- | Add 0-padding at the head of an input list of bits
-- >>> make 4 [1,1] = [0,0,1,1]
make :: Padding -> [Bit] -> [Bit]
make p bits 
    | p > length bits = replicate delta 0 ++ bits
    | otherwise = bits
    where m = length bits
          delta = p - m

-- | int2bin with padding
int2binPad p n = make p $ int2bin n


makeR :: Int -> [Bit] -> [Bit]
makeR n bits = take n (bits ++ repeat 0)

-- >>> makeR 8 [1,1,0] = [1,1,0,0,0,0,0,0]
make8R :: [Bit] -> [Bit]
make8R bits = makeR 8 bits
