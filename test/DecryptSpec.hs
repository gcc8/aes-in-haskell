{-# LANGUAGE OverloadedStrings #-}

module DecryptSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import EncryptBlock
import DecryptBlock

-- | Input data
masterKey = [[0x00, 0x11, 0x22, 0x33], [0x44, 0x55, 0x66, 0x77], [0x88, 0x99, 0xAA, 0xBB], [0xCC, 0xDD, 0xEE, 0xFF]]
block = [[248,249,250,251], [252,253,254,255], [56,57,65,66], [67,68,69,70]]

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

    describe "Tests for Decrypt" $ do
        it "Encrypt, then decrypt (1 round)" $ property propEncryptDecrypt1

    describe "Tests for Decrypt" $ do
        it "Encrypt, then decrypt (10 rounds)" $ property propEncryptDecrypt10

-- | Property to be verified
-- encrypt followed by decrypt should give the original message/block
propEncryptDecrypt1 = decryptedBlock == block
    where nbRounds = 1
          encryptedBlock = encrypt nbRounds masterKey block
          decryptedBlock = decrypt nbRounds masterKey encryptedBlock

propEncryptDecrypt10 = decryptedBlock == block
    where nbRounds = 10
          encryptedBlock = encrypt nbRounds masterKey block
          decryptedBlock = decrypt nbRounds masterKey encryptedBlock
    