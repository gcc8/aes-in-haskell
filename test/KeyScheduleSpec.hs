{-# LANGUAGE OverloadedStrings #-}

module KeyScheduleSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import KeySchedule

-- | Input data
-- The main key has size 128 bits = 16 * 8 bits = 16 bytes = 4x4 matrix of bytes
masterKey = [[0x00, 0x11, 0x22, 0x33], [0x44, 0x55, 0x66, 0x77], [0x88, 0x99, 0xAA, 0xBB], [0xCC, 0xDD, 0xEE, 0xFF]]
-- Known output data
-- Corresponding round keys obtained from the master key
key1  = [[0xf4, 0xe5, 0xc7, 0xf4], [0xae, 0xfb, 0x9d, 0xea], [0x9e, 0x07, 0xad, 0x16], [0x0f, 0xd2, 0x3c, 0xc3]]
key10 = [[0xd7, 0xbd, 0xc4, 0xaf], [0x42, 0xe4, 0xb4, 0x1d], [0x96, 0x17, 0x79, 0x5b], [0xed, 0x15, 0x9f, 0xfc]]
nbRounds = 10

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

    describe "Unit tests for KeySchedule" $ do
        it "Round Key #1 value" $
            computedKey1 `shouldBe` key1

        describe "Unit tests for KeySchedule" $ do
            it "Round Key #10 value" $
                computedKey10 `shouldBe` key10
                    
-- Calculation to be verified
computedRoundKeys = getRoundKeys nbRounds masterKey
computedKey1 = computedRoundKeys !! 0
computedKey10 = computedRoundKeys !! (nbRounds - 1)

