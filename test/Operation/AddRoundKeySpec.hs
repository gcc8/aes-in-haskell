{-# LANGUAGE OverloadedStrings #-}

module Operation.AddRoundKeySpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Operation.AddRoundKey

-- | Input data
key = [[0x00, 0x11, 0x22, 0x33], [0x44, 0x55, 0x66, 0x77], [0x88, 0x99, 0xAA, 0xBB], [0xCC, 0xDD, 0xEE, 0xFF]]
block = [[248,249,250,251],[252,253,254,255],[56,57,65,66],[67,68,69,70]]

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

    describe "Tests for AddRoundKey" $ do
        it "AddRoundKey reversibility" $ property propAddRoundKey

-- | Property to be verified
-- addRoundKey composed with itself should give the identity
propAddRoundKey = computedBlock == block
    where computedBlock = addRoundKey key . addRoundKey key $ block
