{-# LANGUAGE OverloadedStrings #-}

module Operation.MixColumnsSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Operation.MixColumns

-- | Input data
block = [[0x00, 0x11, 0x22, 0x33], [0x44, 0x55, 0x66, 0x77], [0x88, 0x99, 0xAA, 0xBB], [0xCC, 0xDD, 0xEE, 0xFF]]

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

    describe "Tests for MixColumns" $ do
        it "MixColumns reversibility" $ property propMixColumns

-- | Property to be verified
-- mixColumns composed with its inverse should give the identity
propMixColumns = computedBlock == block
    where computedBlock = mixColumnsInv . mixColumns $ block
